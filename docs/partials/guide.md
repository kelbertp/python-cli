## Python CLI Boilerplate Template

This repository houses a starting template for Python CLI projects. It includes linting configuration, Taskfiles, and some simple boilerplate code.

Find libraries we recommend including by scanning through the `pyproject.toml` file.